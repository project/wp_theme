<?php

/**
 * Implementation of hook_views_handlers().
 */
function wp_theme_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'wp_theme'),
    ),
    'handlers' => array(
      'wp_themes_handler_filter_wp_theme_active' => array('parent' => 'views_handler'),
    ),
  );
}

/**
 * Implementation of hook_views_plugins().
 */
function wp_theme_views_plugins() {
  $plugins = array(
    'module' => 'wp_theme',
    'style' => array(
      'have_posts' => array(
        'handler' => 'wp_themes_plugin_style_have_posts',
        'no ui' => TRUE,
      ),
    ),
  );

  return $plugins;
}

/**
 * Implementation of hook_views_data().
 */
function wp_theme_views_data() {
  $data = array();
  // This defines a fake "pseudo" table.
  $data['wp_theme']['table']['group']  = t('WP Theme');
  $data['wp_theme']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'id',
    ),
  );
  $data['wp_theme']['wp_theme_active'] = array(
    'title' => t('Have posts'),
    'help' => t('Runs the content through the standard WordPress theme if enabled.'),
    'filter' => array(
      'handler' => 'wp_themes_handler_filter_wp_theme_active',
    ),
  );

  return $data;
}
