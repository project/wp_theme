<?php

/**
 * Settings form.
 */
function wp_theme_settings_form(&$form_state) {
  $form = array();
  if (module_exists('taxonomy') && $vocabularies = taxonomy_get_vocabularies()) {
    $options = array(0 => t('- None -'));
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->vid] = $vocabulary->name;
    }
    $form['wp_theme_category_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Category vocabulary'),
      '#options' => $options,
      '#default_value' => variable_get('wp_theme_category_vocabulary', 0),
    );
    $form['wp_theme_tags_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Tags vocabulary'),
      '#options' => $options,
      '#default_value' => variable_get('wp_theme_tags_vocabulary', 0),
    );
  }
  return system_settings_form($form);
}

/**
 * Menu callback; displays a listing of all themes.
 *
 * @ingroup forms
 * @see wp_theme_themes_form_submit()
 */
function wp_theme_themes_form() {
  include_once drupal_get_path('module', 'system') . '/system.admin.inc';
  $themes = wp_theme_reload();

  $query = db_query("SELECT * FROM {system} WHERE type = '%s' AND throttle = %s", 'theme', 0);
  while ($theme = db_fetch_object($query)) {
    $theme->info = unserialize($theme->info);
    $themes[$theme->name] = $theme;
  }

  uasort($themes, 'system_sort_modules_by_info_name');

  $status = array();
  $incompatible_core = array();
  $incompatible_php = array();

  foreach ($themes as $theme) {
    $screenshot = NULL;
    // Create a list which includes the current theme and all its base themes.
    if (isset($themes[$theme->name]->base_themes)) {
      $theme_keys = array_keys($themes[$theme->name]->base_themes);
      $theme_keys[] = $theme->name;
    }
    else {
      $theme_keys = array($theme->name);
    }
    // Look for a screenshot in the current theme or in its closest ancestor.
    foreach (array_reverse($theme_keys) as $theme_key) {
      if (isset($themes[$theme_key]) && file_exists($themes[$theme_key]->info['screenshot'])) {
        $screenshot = $themes[$theme_key]->info['screenshot'];
        break;
      }
    }
    $screenshot = $screenshot ? theme('image', $screenshot, t('Screenshot for %theme theme', array('%theme' => $theme->info['name'])), '', array('class' => 'screenshot'), FALSE) : t('no screenshot');

    $form[$theme->name]['screenshot'] = array('#value' => $screenshot);
    $form[$theme->name]['info'] = array(
      '#type' => 'value',
      '#value' => $theme->info,
    );
    $options[$theme->name] = '';

    if (!empty($theme->status) || $theme->name == variable_get('admin_theme', '0')) {
      $form[$theme->name]['operations'] = array('#value' => l(t('configure'), 'admin/build/themes/settings/'. $theme->name) );
    }
    else {
      // Dummy element for drupal_render. Cleaner than adding a check in the theme function.
      $form[$theme->name]['operations'] = array();
    }
    if (!empty($theme->status)) {
      $status[] = $theme->name;
    }
    else {
      // Ensure this theme is compatible with this version of core.
      if (!isset($theme->info['core']) || $theme->info['core'] != DRUPAL_CORE_COMPATIBILITY) {
        $incompatible_core[] = $theme->name;
      }
      if (version_compare(phpversion(), $theme->info['php']) < 0) {
        $incompatible_php[$theme->name] = $theme->info['php'];
      }
    }
  }

  $form['status'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $status,
    '#incompatible_themes_core' => drupal_map_assoc($incompatible_core),
    '#incompatible_themes_php' => $incompatible_php,
  );
  $form['theme_default'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => variable_get('theme_default', 'garland'),
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['buttons']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to defaults'),
  );
  $form['#theme'] = 'system_themes_form';
  $form['#submit'] = array('wp_theme_system_themes_form_submit', 'system_themes_form_submit');
  return $form;
}

function wp_theme_system_themes_form_submit($form, &$form_state) {
  $statuses = array();
  if ($form_state['values']['op'] == t('Save configuration')) {
    if (is_array($form_state['values']['status'])) {
      foreach ($form_state['values']['status'] as $key => $choice) {
        // Always enable the default theme, despite its status checkbox being checked:
        if ($choice || $form_state['values']['theme_default'] == $key) {
          $statuses[$key] = TRUE;
        }
      }
    }
  }
  else {
    $statuses['garland'] = TRUE;
  }
  variable_set('wp_theme_statuses', $statuses);
}
