<?php
define('TEMPLATEPATH', path_to_theme() . '/twentyten');

function add_action($action, $function = NULL, $remove = FALSE) {
  static $actions = array();
  if ($function) {
    if (!$remove) {
      $actions[$action][] = $function;
    }
    else {
      if (!empty($actions[$action])) {
        foreach ($actions[$action] as $key => $f) {
          if ($f == $function) {
            unset($actions[$action][$key]);
            break;
          }
        }
      }
    }
  }
  if (isset($actions[$action])) {
    return $actions[$action];
  }
}
function register_nav_menus() {
  // Will have to implement at some point...
}
function add_custom_background() {
  // Also have to implement.
}
function apply_filters($type, $text) {
  return $text;
}
function set_post_thumbnail_size() {}
function add_custom_image_header() {}
function register_default_headers($headers = NULL) {
  return wp_theme_static('headers', $headers);
}

function add_filter() {}
function __($text) {
  if ($text == 'Semantic Personal Publishing Platform') {
    $text = 'Powered by Drupal';
  }
  elseif ($text == 'Proudly powered by %s.') {
    $text = '<img src="' . base_path() . '/misc/powered-blue-80x15.png"';
  }
  elseif ($text == 'http://wordpress.org/') {
    $text = 'http://drupal.org/';
  }
  return t($text);
}
function _e($text) { echo __($text); }
function _x($text) { return __($text); }
function _ex($text) { echo __($text); }
function _n($one, $other, $count) { return format_plural($count, $one, $other); }
function number_format_i18n($n) { return $n; }
function do_action() {}
function register_sidebar($sidebar = NULL) {
  static $sidebars = array();
  if ($sidebar) {
    $sidebars[$sidebar['id']] = $sidebar;
  }
  return $sidebars;
}
function remove_action($action, $function) {
  add_action($action, $function, TRUE);
}
function add_editor_style() {}
function add_theme_support($item = NULL) {
  static $items;
  if (isset($item)) {
    $items[] = $item;
  }
  return $items;
}

function load_theme_textdomain() {}

function get_header($name = NULL) {
  get_template_part('header', $name);
}

function get_footer($name = NULL) {
  get_template_part('footer', $name);
}

function language_attributes() {
  print ' xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $GLOBALS['language']->language .
    '" lang="' . $GLOBALS['language']->language . '" dir="' . ($GLOBALS['language']->direction ? 'rtl' : 'ltr') . '"';
}
function bloginfo($info) {
  switch ($info) {
    case 'charset':
      print 'text/html; charset=utf-8';
      break;
    case 'name':
      print variable_get('site_name', 'Drupal');
      break;
    case 'description':
      print variable_get('site_slogan', '');
  }
}
function wp_title($separator, $display = TRUE) {
  if (!drupal_get_title()) {
    return;
  }
  $title = drupal_get_title() . ' ' . $separator . ' ';
  if ($display) {
    print $title;
  }
  else {
    return $title;
  }
}
function get_bloginfo($show = '') {
  $theme_base = drupal_get_path('theme', _wp_theme_name()) . '/' . _wp_theme_name();
  switch($show) {
    case 'url' :
    case 'wpurl' :
      $output = base_url;
      break;
    case 'description':
      $output = variable_get('site_slogan', '');
      break;
    case 'rdf_url':
      $output = get_feed_link('rdf');
      break;
    case 'rss_url':
      $output = get_feed_link('rss');
      break;
    case 'rss2_url':
      $output = get_feed_link('rss2');
      break;
    case 'atom_url':
      $output = get_feed_link('atom');
      break;
    case 'comments_atom_url':
      $output = get_feed_link('comments_atom');
      break;
    case 'comments_rss2_url':
      $output = get_feed_link('comments_rss2');
      break;
    case 'pingback_url':
      $output = get_option('siteurl') .'/xmlrpc.php';
      break;
    case 'stylesheet_url':
      $output = $theme_base . '/style.css';
      break;
    case 'stylesheet_directory':
    case 'template_directory':
    case 'template_url':
      $output = $theme_base;
      break;
    case 'admin_email':
      $output = get_option('admin_email');
      break;
    case 'charset':
      $output = get_option('blog_charset');
      if ('' == $output) $output = 'UTF-8';
      break;
    case 'html_type' :
      $output = get_option('html_type');
      break;
    case 'version':
      global $wp_version;
      $output = $wp_version;
      break;
    case 'language':
      $output = $GLOBALS['language']->language;
      break;
    case 'name':
    default:
      $output = variable_get('site_name', 'Drupal');
      break;
  }

  return $output;
}

function is_home() {
  return drupal_is_front_page();
}

function is_front_page() {
  return drupal_is_front_page();
}

function is_singular() {
  return (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2));
}

function get_option($option) {
  switch ($option) {
    case 'thread_comments':
      return FALSE;
  }
}

function wp_theme_css_stripped($match = array('modules/*'), $exceptions = NULL) {
  // Set default exceptions
  if (!is_array($exceptions)) {
    $exceptions = array(
      //'modules/system/system.css',
      'modules/update/update.css',
      'modules/openid/openid.css',
      'modules/block/block.css',
      'modules/acquia/*',
    );
  }
  $css = drupal_add_css();
  $match = implode("\n", $match);
  $exceptions = implode("\n", $exceptions);
  foreach (array_keys($css['all']['module']) as $filename) {
    if (drupal_match_path($filename, $match) && !drupal_match_path($filename, $exceptions)) {
      unset($css['all']['module'][$filename]);
    }
  }
  // Our system.css override.
  $css['all']['module'][drupal_get_path('module', 'wp_theme') . '/system.css'] = 1;
  $css['all']['theme'][drupal_get_path('module', 'wp_theme') . '/wp_theme.css'] = 1;

  return $css;
}

function wp_head() {
  print drupal_get_css(wp_theme_css_stripped()) . drupal_get_js() . drupal_get_html_head();
}


function home_url() {
  return base_path();
}

function esc_attr($text) {
  return check_plain($text);
}

function esc_attr_e($text) {
  return check_plain(__($text));
}
function esc_attr__($text) {
  return check_plain(__($text));
}
function esc_url($text) {
  return $text;
}
function wp_nav_menu() {
  print '<div class="menu">' . theme('links', menu_primary_links()) . '</div>';
}

function has_post_thumbnail() {
  return FALSE;
}

function wp_get_attachment_image_src() {
  return FALSE;
}

function header_image() {
  $headers = register_default_headers();
  // Come up with some choosing logic.
  $header = array_shift($headers);
  printf($header['url'], base_path() . path_to_theme() . '/twentyten');
}

function body_class() {
  return '';
}

function get_sidebar($name = NULL) {
  get_template_part('sidebar', $name);
}

function dynamic_sidebar($area) {
  // Return the blocks for the $area.
  $region = str_replace('-', '_', $area);
  $blocks = block_list($region);
  if (empty($blocks)) {
    return FALSE;
  }
  $output = '';
  foreach ($blocks as $key => $block) {
    $output .= theme('block', $block);
  }
  print $output;
  return TRUE;
}

function is_active_sidebar($area) {
  $region = str_replace('-', '_', $area);
  $blocks = block_list($region);
  return !empty($blocks);
}

function get_template_part($slug, $name) {
  global $theme;
  $theme_base = drupal_get_path('theme', $theme) . '/' . _wp_theme_name();
  $suggestions = array();
  if ($name) {
    $suggestions[] = $theme_base . '/' . $slug . '-' . $name . '.php';
  }
  $suggestions[] = $theme_base . '/' . $slug . '.php';
  foreach ($suggestions as $suggestion) {
    if (wp_theme_file_exists($suggestion)) {
      wp_theme_include($suggestion);
      break;
    }
  }
}

function have_posts() {
  return wp_theme_posts('have');
}

function the_post() {
  return wp_theme_posts('next');
}

function in_category() {
  // TODO: implement
  return FALSE;
}

function the_ID() {
  $post = wp_theme_posts();
  print $post->nid;
}

function post_class() {
  $post = wp_theme_posts();

  $classes = array();

  $classes[] = 'post-' . $post->nid;
  $classes[] = 'type-' . $post->type;

  if ($post->sticky) {
    $classes[] = 'sticky';
  }

  $classes[] = 'hentry';

  // Categories
  /*
  foreach ( (array) get_the_category($post->ID) as $cat ) {
    if ( empty($cat->slug ) )
      continue;
    $classes[] = 'category-' . sanitize_html_class($cat->slug, $cat->cat_ID);
  }

  // Tags
  foreach ( (array) get_the_tags($post->ID) as $tag ) {
    if ( empty($tag->slug ) )
      continue;
    $classes[] = 'tag-' . sanitize_html_class($tag->slug, $tag->term_id);
  }
  */
  print 'class="' . implode(' ', $classes) . '"';
}

function get_permalink() {
  $post = wp_theme_posts();
  return url('node/' . $post->nid);
}

function the_permalink() {
  print get_permalink();
}

function get_the_time() {
  $post = wp_theme_posts();
  return date('g:i a', $post->created);
}

function get_the_date() {
  $post = wp_theme_posts();
  return date('F j, Y', $post->created);
}

function the_title_attribute($args) {
  $args = wp_parse_args($args);
  $post = wp_theme_posts();
  $title = $post->title;
  if ($args['echo']) {
    echo $title;
  }
  else {
    return $title;
  }
}

function the_title() {
  echo get_the_title();
}

function get_the_title() {
  $post = wp_theme_posts();
  $title = $post->title;
  return check_plain($title);
}

function wp_parse_args($args) {
  if (is_object($args)) {
    $r = get_object_vars($args);
  }
  elseif (is_array($args)) {
    $r =& $args;
  }
  else {
    parse_str($args, $r);
  }
  return $r;
}

function get_author_posts_url($uid) {
  return url('user/' . $uid);
}

function get_the_author() {
  return get_the_author_meta('name');
}
function get_the_author_meta($data) {
  $post = wp_theme_posts();
  if ($data == 'ID') {
    return $post->uid;
  }
  elseif ($data == 'name') {
    return check_plain($post->name);
  }
}

function post_password_required() {
  // We don't support this feature yet.
  return FALSE;
}

function get_children() {
  // Don't support this either, yet
  return array();
}

function the_excerpt() {
  $post = wp_theme_posts();
  print $post->teaser;
}

function is_archive() {
  return FALSE;
}

function is_search() {
  return FALSE;
}

function the_content() {
  $post = wp_theme_posts();
  if (arg(0) == 'node' && arg(1) == $post->nid) {
    print $post->body;
  }
  else {
    print $post->teaser;
  }
}

function wp_link_pages() {
  print '';
}

function get_the_category() {
  $post = wp_theme_posts();
  $vocabulary = variable_get('wp_theme_category_vocabulary', 0);
  if ($vocabulary) {
    $terms = array();
    foreach ($post->taxonomy as $term) {
      if ($term->vid == $vocabulary) {
        $terms[] = $term;
      }
    }
    return $terms;
  }
}

function get_the_category_list($separator = ', ') {
  $links = array();
  $categories = get_the_category();
  if (!empty($categories)) {
    foreach ($categories as $term) {
      $links[] = l($term->name, taxonomy_term_path($term));
    }
    return implode($separator, $links);
  }
}

function the_category() {
  print get_the_category_list();
}


function get_search_form() {
  return FALSE;
}

function get_the_tag_list($before = '', $separator = ', ', $after = '') {
  $post = wp_theme_posts();
  $link = array();
  $vocabulary = variable_get('wp_theme_tags_vocabulary', 0);
  if ($vocabulary) {
    foreach ($post->taxonomy as $term) {
      if ($term->vid == $vocabulary) {
        $links[] = $before . l($term->name, taxonomy_term_path($term)) . $after;
      }
    }
  }
  if (!empty($links)) {
    return implode($separator, $links);
  }
}

function the_tags() {
  print get_the_tag_list();
}

function comments_open() {
  $post = wp_theme_posts();
  return ($post->comment == COMMENT_NODE_READ_WRITE);
}

function comments_popup_link($zero = FALSE, $one = FALSE, $more = FALSE, $css_class = '', $none = FALSE) {
  if (!$zero) {
    $zero = t('No Comments');
  }
  if (!$one) {
    $one = t('One comment');
  }
  if (!$more) {
    $more = t('% comments');
  }
  if (!$none) {
    $none = t('Comments Off');
  }

  $post = wp_theme_posts();
	$number = comment_num_all($post->nid);

	if ($number == 0 && $post->comment != COMMENT_NODE_READ_WRITE) {
		echo '<span' . ((!empty($css_class)) ? ' class="' . esc_attr($css_class) . '"' : '') . '>' . $none . '</span>';
		return;
	}

  /*
	if ( post_password_required() ) {
		echo __('Enter your password to view comments.');
		return;
	}
	*/

	echo '<a href="' . get_permalink() . ($number == 0? '#comment-form' : '#comments') . '"';

	if (!empty($css_class)) {
		echo ' class="' . $css_class . '" ';
	}
	$title = the_title_attribute(array('echo' => 0 ));

	echo ' title="' . t('Comment on %title', array('%title' => $title)) . '">';

	if ($number == 0) {
	  echo $zero;
	}
	elseif ($number == 1) {
	  echo $one;
	}
	else {
	  echo str_replace('%', $number, $more);
	}

	echo '</a>';
}

function edit_post_link($text, $prefix = '') {
  $post = wp_theme_posts();
  if ($post->nid != -1 && node_access('update', $post)) {
    echo $prefix . l($text, 'node/' . $post->nid . '/edit');
  }
}

function comments_template($a = NULL, $echo = TRUE) {
  $post = wp_theme_posts();
  if ((!is_single() && !is_page()) || $post->nid == -1) {
    return;
  }
  $comments = wp_theme_include(path_to_theme() . '/' . _wp_theme_name() . '/comments.php', array());
  if ($echo) {
    echo $comments;
  }
  if (!$echo) {
    return $comments;
  }
}

function wp_footer() {
  print theme('closure');
}

function previous_post_link() {
  return '';
}

function next_post_link() {
  return '';
}

function get_locale() {
  return $GLOBALS['language']->language;
}

function is_object_in_taxonomy() {
  return FALSE;
}

function get_post_type() {

}

function get_template_directory() {
  return wp_theme_active();
}
function get_template_directory_uri() {
  return base_path() . get_template_directory();
}
function add_image_size() {}
function is_admin() {}
function add_shortcode() {}
function wp_get_archives() {}
function has_nav_menu() {}
function single_cat_title($a, $echo) {
  $category_title = wp_theme_static('category_title');
  if (!$category_title) {
    return;
  }
  if ($echo) {
    echo check_plain($category_title);
  }
  else {
    return check_plain($category_title);
  }
}
function single_tag_title($a, $echo) {
  $tag_title = wp_theme_static('tag_title');
  if (!$tag_title) {
    return;
  }
  if ($echo) {
    echo check_plain($tag_title);
  }
  else {
    return check_plain($tag_title);
  }
}
function category_description() {}

function have_comments() {
  $post = wp_theme_posts();
  return $post->comment_count;
}
function get_comments_number() {
  return have_comments();
}
function get_comment_pages_count() {
  
  return 1; // For now, can't figure how to o this differently.
}
function wp_list_comments($options = array()) {
  static $list;
  if (user_access('access comments') && !isset($list)) {
    global $user;
    $node = wp_theme_posts();
    $output = '';

    $mode = _comment_get_display_setting('mode', $node);
    $order = _comment_get_display_setting('sort', $node);
    $comments_per_page = 10000000 /* _comment_get_display_setting('comments_per_page', $node) */;

    // Multiple comment view
    $query_count = 'SELECT COUNT(*) FROM {comments} c WHERE c.nid = %d';
    $query = 'SELECT c.cid as cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp, c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature, u.signature_format, u.picture, u.data, c.thread, c.status FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE c.nid = %d';
    $query_args = array($node->nid);

    if (!user_access('administer comments')) {
      $query .= ' AND c.status = %d';
      $query_count .= ' AND c.status = %d';
      $query_args[] = COMMENT_PUBLISHED;
    }

    if ($order == COMMENT_ORDER_NEWEST_FIRST) {
      if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
        $query .= ' ORDER BY c.cid DESC';
      }
      else {
        $query .= ' ORDER BY c.thread DESC';
      }
    }
    else if ($order == COMMENT_ORDER_OLDEST_FIRST) {
      if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
        $query .= ' ORDER BY c.cid';
      }
      else {
        // See comment above. Analysis reveals that this doesn't cost too
        // much. It scales much much better than having the whole comment
        // structure.
        $query .= ' ORDER BY SUBSTRING(c.thread, 1, (LENGTH(c.thread) - 1))';
      }
    }
    $query = db_rewrite_sql($query, 'c', 'cid');
    $query_count = db_rewrite_sql($query_count, 'c', 'cid');

    // Start a form, for use with comment control.
    $result = pager_query($query, $comments_per_page, 0, $query_count, $query_args);

    $list = array();
    while ($comment = db_fetch_object($result)) {
      $comment = drupal_unpack($comment);
      $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
      $comment->depth = count(explode('.', $comment->thread)) - 1;
      $comment->comment_type = '';
      $comment->comment_approved = !$comment->status;
      $comment->comment_ID = $comment->cid;
      $list[] = $comment;
    }
  }
  if (user_access('access comments')) {
    if ($options['callback']) {
      foreach ($list as $comment) {
        wp_theme_static('comment', $comment);
        $options['callback']($comment, array('max_depth' => 0), $comment->depth);
      }
    }
    return $list;
  }
  return array();
}

function comment_class() {}
function comment_ID() {
  $comment = wp_theme_static('comment');
  return $comment->cid;
}
function get_comment_author_link() {
  $comment = wp_theme_static('comment');
  return l($comment->name, 'user/' . $comment->uid);
}
function get_comment_link() {
  $comment = wp_theme_static('comment');
  return url('node/' . $comment->nid, array('fragment' => 'comment-' . $comment->cid));
}
function get_avatar() {}
function get_comment_date() {
  $comment = wp_theme_static('comment');
  return date('F jS', $comment->timestamp);
}
function get_comment_time() {
  $comment = wp_theme_static('comment');
  return date('G:i a', $comment->timestamp);
}
function edit_comment_link($link, $prefix) {
  $comment = wp_theme_static('comment');
  print $prefix . l($link, 'comment/edit/' . $comment->cid);
}
function comment_text() {
  $comment = wp_theme_static('comment');
  print $comment->comment;
}
function comment_reply_link() {
  
}
function comments_number() {
  print count(wp_list_comments());
}
function wp_theme_alias_comment_form() {
  $post = wp_theme_posts();
  print comment_form_box(array('nid' => $post->nid), t('Post new comment'));
}
function is_single() {
  return (arg(0) == 'node' && is_numeric(arg(1)));
}
function is_page() {
  $post = wp_theme_posts();
  if ($post->nid == -1) {
    return TRUE;
  }
  return (arg(0) == 'node' && is_numeric(arg(1)));
}
function is_sticky() {
  $post = wp_theme_posts();
  return $post->sticky;
}
function the_author() {
  print 'me';
}
function the_time() {
  print 'now';
}
function allowed_tags() {
  return '';
}