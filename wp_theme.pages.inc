<?php

/**
 * Menu callback; Generate a listing of promoted nodes.
 */
function wp_theme_node_page_default() {
  if (!wp_theme_active()) {
    return node_page_default();
  }
  $result = pager_query(db_rewrite_sql('SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.promote = 1 AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC'), variable_get('default_nodes_main', 10));

  $posts = array();
  $num_rows = FALSE;
  while ($node = db_fetch_object($result)) {
    $posts[] = $node->nid;
    $num_rows = TRUE;
  }

  if ($num_rows) {
    $feed_url = url('rss.xml', array('absolute' => TRUE));
    drupal_add_feed($feed_url, variable_get('site_name', 'Drupal') .' '. t('RSS'));
    $output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
  }
  else {
    $default_message = t('<h1 class="title">Welcome to your new Drupal website!</h1><p>Please follow these steps to set up and start using your website:</p>');
    $default_message .= '<ol>';

    $default_message .= '<li>'. t('<strong>Configure your website</strong> Once logged in, visit the <a href="@admin">administration section</a>, where you can <a href="@config">customize and configure</a> all aspects of your website.', array('@admin' => url('admin'), '@config' => url('admin/settings'))) .'</li>';
    $default_message .= '<li>'. t('<strong>Enable additional functionality</strong> Next, visit the <a href="@modules">module list</a> and enable features which suit your specific needs. You can find additional modules in the <a href="@download_modules">Drupal modules download section</a>.', array('@modules' => url('admin/build/modules'), '@download_modules' => 'http://drupal.org/project/modules')) .'</li>';
    $default_message .= '<li>'. t('<strong>Customize your website design</strong> To change the "look and feel" of your website, visit the <a href="@themes">themes section</a>. You may choose from one of the included themes or download additional themes from the <a href="@download_themes">Drupal themes download section</a>.', array('@themes' => url('admin/build/themes'), '@download_themes' => 'http://drupal.org/project/themes')) .'</li>';
    $default_message .= '<li>'. t('<strong>Start posting content</strong> Finally, you can <a href="@content">create content</a> for your website. This message will disappear once you have promoted a post to the front page.', array('@content' => url('node/add'))) .'</li>';
    $default_message .= '</ol>';
    $default_message .= '<p>'. t('For more information, please refer to the <a href="@help">help section</a>, or the <a href="@handbook">online Drupal handbooks</a>. You may also post at the <a href="@forum">Drupal forum</a>, or view the wide range of <a href="@support">other support options</a> available.', array('@help' => url('admin/help'), '@handbook' => 'http://drupal.org/handbooks', '@forum' => 'http://drupal.org/forum', '@support' => 'http://drupal.org/support')) .'</p>';

    $output = '<div id="first-time">'. $default_message .'</div>';
  }
  drupal_set_title('');
  wp_theme_posts('put', array('nodes' => $posts));

  return $output;
}


/**
 * Menu callback; displays all nodes associated with a term.
 */
function wp_theme_taxonomy_term_page($str_tids = '', $depth = 0, $op = 'page') {
  if (!wp_theme_active()) {
    include_once drupal_get_path('module', 'taxonomy') . '/taxonomy.pages.inc';
    return taxonomy_term_page($str_tids, $depth, $op);
  }
  $terms = taxonomy_terms_parse_string($str_tids);
  if ($terms['operator'] != 'and' && $terms['operator'] != 'or') {
    drupal_not_found();
  }

  if ($terms['tids']) {
    $result = db_query(db_rewrite_sql('SELECT t.tid, t.name, t.vid FROM {term_data} t WHERE t.tid IN ('. db_placeholders($terms['tids']) .')', 't', 'tid'), $terms['tids']);
    $tids = array(); // we rebuild the $tids-array so it only contains terms the user has access to.
    $names = array();
    $category = variable_get('wp_theme_category_vocabulary', 0);
    $tags = variable_get('wp_theme_tags_vocabulary', 0);
    $page_type = '';
    while ($term = db_fetch_object($result)) {
      $tids[] = $term->tid;
      $names[] = $term->name;
      if ($term->vid == $category) {
        $page_type = 'category';
        $category_title = wp_theme_static('category_title', $term->name);
      }
      elseif ($term->vid == $tags) {
        $page_type = 'tag';
        $category_title = wp_theme_static('tag_title', $term->name);
      }
    }

    if ($names) {
      $title = implode(', ', $names);
      drupal_set_title(check_plain($title));

      switch ($op) {
        case 'page':
          $result = taxonomy_select_nodes($tids, $terms['operator'], $depth, FALSE);
          $items = array(); 
          while ($row = db_fetch_object($result)) {
            $items[] = $row->nid;
          }
          wp_theme_posts('put', array('nodes' => $items));
          wp_theme_static('page_type', $page_type);
          return '';

        case 'feed':
          include_once drupal_get_path('modules', 'taxonomy') . '/taxonomy.pages.inc';
          taxonomy_term_page($str_tids, $depth, $op);
          break;

        default:
          drupal_not_found();
      }
    }
    else {
      drupal_not_found();
    }
  }
}