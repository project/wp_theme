<?php

class wp_themes_handler_filter_wp_theme_active extends views_handler {
  function query() {
    if (wp_theme_active()) {
      // Override the style and shove the results into have_posts().

      // Tell other people what we're doing.
      $this->view->build_info['have_posts'] = TRUE;
      // Hijack the style plugin and change it to 'have_posts', which
      // is a special, hidden style plugin.
      $this->view->plugin_name = 'have_posts';
      $this->query->add_field($this->table_alias, 'nid', 'wp_themes_nid');
    }
  }
}

