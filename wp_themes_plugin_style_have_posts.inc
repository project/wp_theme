<?php

class wp_themes_plugin_style_have_posts extends views_plugin_style {
  function uses_row_plugin() {
    return FALSE;
  }
  function uses_fields() {
    return FALSE;
  }
  function render() {
    $rows = array();
    foreach ($this->view->result as $item) {
      $rows[] = $item->wp_themes_nid;
    }
    wp_theme_posts('put', array('nodes' => $rows));
  }
}
